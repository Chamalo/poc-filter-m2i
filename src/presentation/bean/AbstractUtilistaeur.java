package presentation.bean;

import java.util.List;

import org.apache.struts.action.Action;

/**
 * AbstractactUtilisateur abstract class; extends {@link org.apache.struts.action.Action}
 * 
 * @author Valentin J.
 */
public abstract class AbstractUtilistaeur extends Action {
	/**
	 * Method to find a Utilisateur
	 * 
	 * @param  listUtilisateur List of UtilisateurDto
	 * @param  id              ID of Utilisateur
	 * @return                 Utilisateur if find, null otherwise
	 */
	protected UtilisateurDto findUtilisateur(final List<UtilisateurDto> listUtilisateur, final int id) {
		for (final UtilisateurDto utilisateur : listUtilisateur) {
			if (utilisateur.getId() == id) {
				return utilisateur;
			}
		}
		return null;
	}
}
