package presentation.bean;

import java.io.Serializable;

import presentation.roles.ERoles;

/**
 * UtilisateurDto class; implements {@link java.io.Serializable}
 * 
 * @author Valentin J.
 */
public class UtilisateurDto implements Serializable {

	private static final long serialVersionUID = 3654297383714722490L;
	private int               id;
	private String            nom;
	private String            prenom;
	private String            pwd;
	private ERoles            role;

	/**
	 * Constructeur par d�faut
	 */
	public UtilisateurDto() {
		// empty method
	}

	/**
	 * Permet d'initialiser un utilisateur
	 * 
	 * @param  idUser     l'id de l'utilisateur
	 * @param  nomUser    le nom de l'utilisateur
	 * @param  prenomUser le prenom de l'utilisateur
	 * @param  pwdUser    Mot de passe de l'utilisateur
	 * @param  roleUser   Role de l'utilisateur
	 * @return            l'utilisateur initialis�
	 */
	public UtilisateurDto initUtilisateur(final int idUser, final String nomUser, final String prenomUser, final String pwdUser,
			final ERoles roleUser) {
		this.id = idUser;
		this.nom = nomUser;
		this.prenom = prenomUser;
		this.pwd = pwdUser;
		this.role = roleUser;
		return this;
	}

	/**
	 * Getter for id
	 *
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Setter for id
	 *
	 * @param id the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Getter for nom
	 *
	 * @return the nom
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Setter for nom
	 *
	 * @param nom the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Getter for prenom
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return this.prenom;
	}

	/**
	 * Setter for prenom
	 *
	 * @param prenom the prenom to set
	 */
	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Get the value of pwd
	 *
	 * @return the pwd
	 */
	public String getPwd() {
		return this.pwd;
	}

	/**
	 * Set new value to pwd
	 *
	 * @param pwd the pwd to set
	 */
	public void setPwd(final String pwd) {
		this.pwd = pwd;
	}

	/**
	 * Get the value of role
	 *
	 * @return the role
	 */
	public ERoles getRole() {
		return this.role;
	}

	/**
	 * Set new value to role
	 *
	 * @param role the role to set
	 */
	public void setRole(final ERoles role) {
		this.role = role;
	}

}
