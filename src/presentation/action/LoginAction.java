package presentation.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.AbstractUtilistaeur;
import presentation.bean.UtilisateurDto;
import presentation.form.LoginForm;

/**
 * LoginAction class; extends {@link presentation.bean.AbstractUtilistaeur}
 * 
 * @author Valentin J.
 */
public class LoginAction extends AbstractUtilistaeur {

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		final LoginForm loginForm = (LoginForm) form;

		@SuppressWarnings("unchecked")
		final List<UtilisateurDto> listUtilisateur = (List<UtilisateurDto>) request.getSession().getAttribute(InitAction.LIST_UTILISATEURS);

		for (final UtilisateurDto user : listUtilisateur) {
			if (user.getNom().equals(loginForm.getNom()) && user.getPwd().equals(loginForm.getPwd())) {
				request.getSession().setAttribute("user", user);
				return mapping.findForward("sucess");
			}
		}

		return mapping.findForward("sucess");
	}

}
