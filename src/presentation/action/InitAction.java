package presentation.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.UtilisateurDto;
import presentation.roles.ERoles;

/**
 * InitAction class; extends {@link org.apache.struts.action.Action}
 * 
 * @author Valentin J.
 */
public class InitAction extends Action {
	/**
	 * Static name for access session attribute List Utilisateur
	 */
	public static final String LIST_UTILISATEURS = "listUtilisateurs";

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		// Cr�ation des users
		final List<UtilisateurDto> utilisateurs = this.createListUtilisateur();

		// On r�cup�re la session 
		final HttpSession session = request.getSession();

		// On y ajoute les users et la map 
		session.setAttribute(LIST_UTILISATEURS, utilisateurs);

		// On choisi une local par default
		session.setAttribute(Globals.LOCALE_KEY, Locale.FRENCH);

		return mapping.findForward("sucess");
	}

	/**
	 * Method to create a List of Utilisateur
	 * 
	 * @return List<Utilisateur>
	 */
	private List<UtilisateurDto> createListUtilisateur() {
		final List<UtilisateurDto> utilisateurs = new ArrayList<>();

		utilisateurs.add(new UtilisateurDto().initUtilisateur(12, "User 1", "Prenom 1", "test", ERoles.ADMIN));
		utilisateurs.add(new UtilisateurDto().initUtilisateur(7, "User 2", "Prenom 2", "test", ERoles.CLIENT));
		utilisateurs.add(new UtilisateurDto().initUtilisateur(9, "User 3", "Prenom 3", "test", ERoles.CLIENT));

		return utilisateurs;
	}

}
