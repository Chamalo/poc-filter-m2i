package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * LogoutAction class; extends {@link org.apache.struts.action.Action}
 * 
 * @author Valentin J.
 */
public class LogoutAction extends Action {

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		// Attention , il supprime les utilisateurs cr�er, car ils sont stocker en session !
		request.getSession().invalidate();

		return mapping.findForward("sucess");
	}

}
