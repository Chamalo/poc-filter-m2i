package presentation.action;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * LangueAction class; extends {@link org.apache.struts.action.Action}
 * 
 * @author Valentin J.
 */
public class LangueAction extends Action {
	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		final HttpSession session = request.getSession();

		if (session.getAttribute(Globals.LOCALE_KEY) == Locale.FRENCH) {
			session.setAttribute(Globals.LOCALE_KEY, Locale.ENGLISH);
		} else {
			session.setAttribute(Globals.LOCALE_KEY, Locale.FRENCH);
		}

		final String backurl = request.getHeader("referer");

		final String[] urlSplit = backurl.split("/");

		ActionForward forward = new ActionForward();

		forward = mapping.getInputForward();
		forward.setPath(urlSplit[urlSplit.length - 1]);
		forward.setName("sucess");
		forward.setRedirect(true);

		return forward;
	}

}
