package presentation.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import presentation.bean.AbstractUtilistaeur;
import presentation.bean.UtilisateurDto;
import presentation.form.UtilisateurForm;
import presentation.roles.ERoles;

/**
 * CreerAction class; extends {@link presentation.bean.AbstractUtilistaeur}
 * 
 * @author Valentin J.
 */
public class CreerAction extends AbstractUtilistaeur {

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		final UtilisateurForm userForm = (UtilisateurForm) form;

		final UtilisateurDto newUser = new UtilisateurDto().initUtilisateur(Integer.parseInt(userForm.getId()), userForm.getNom(),
				userForm.getPrenom(), userForm.getPwd(), ERoles.CLIENT);

		@SuppressWarnings("unchecked")
		final List<UtilisateurDto> listUtilisateur = (List<UtilisateurDto>) request.getSession().getAttribute(InitAction.LIST_UTILISATEURS);

		if (this.findUtilisateur(listUtilisateur, newUser.getId()) == null) {
			listUtilisateur.add(newUser);
			final ActionMessages messages = new ActionMessages();
			messages.add("userOk", new ActionMessage("add.create.success", new Object[] {newUser.getId()}));
			this.saveMessages(request, messages);
		} else {
			final ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("errors.add.id.used", new Object[] {newUser.getId()}));
			this.saveErrors(request, errors);
		}

		return mapping.findForward("sucess");
	}
}
