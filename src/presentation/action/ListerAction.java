package presentation.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.UtilisateurDto;

/**
 * ListerAction class; extends {@link org.apache.struts.action.Action}
 * 
 * @author Valentin J.
 */
public class ListerAction extends Action {

	/**
	 * Static name for listUtilisaeur
	 */
	public static final String LISTE_USERS_REQUEST = "listeUsers";

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		@SuppressWarnings("unchecked")
		final List<UtilisateurDto> listUtilisateur = (List<UtilisateurDto>) request.getSession().getAttribute(InitAction.LIST_UTILISATEURS);

		request.setAttribute(LISTE_USERS_REQUEST, listUtilisateur);

		return mapping.findForward("sucess");
	}

}
