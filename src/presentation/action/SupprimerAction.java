package presentation.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.AbstractUtilistaeur;
import presentation.bean.UtilisateurDto;

/**
 * SupprimerAction class; extends {@link presentation.bean.AbstractUtilistaeur}
 * 
 * @author Valentin J.
 */
public class SupprimerAction extends AbstractUtilistaeur {

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		final String id = request.getParameter("id");

		@SuppressWarnings("unchecked")
		final List<UtilisateurDto> listUtilisateur = (List<UtilisateurDto>) request.getSession().getAttribute(InitAction.LIST_UTILISATEURS);

		listUtilisateur.remove(this.findUtilisateur(listUtilisateur, Integer.parseInt(id)));

		return mapping.findForward("sucess");
	}

}
