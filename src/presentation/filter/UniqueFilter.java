package presentation.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import presentation.bean.UtilisateurDto;

/**
 * Filter for all app
 * 
 * @author Valentin
 */
//@WebFilter(filterName = "UniqueFilter", urlPatterns = {"/consulter.do"})
public class UniqueFilter implements Filter {

	@Override
	public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain chain)
			throws ServletException, IOException {
		final HttpServletRequest request = (HttpServletRequest) req;
		final HttpServletResponse response = (HttpServletResponse) resp;

		// Recup utilisateur
		final UtilisateurDto user = (UtilisateurDto) request.getSession().getAttribute("user");

		// Si on a un utilisateur
		if (user != null && !user.getNom().isEmpty()) {
			// Si le role donne acc�s a la page
			if (user.getRole().getPages().contains(request.getRequestURI().split("/")[2])) {
				// user.getRole => Recup le role de ERoles
				// getPages => Recup�re la liste des pages / Voir ERoles
				// contains => check s'il contient
				// request.getRequestURI => Permet de savoir quel lien on vise; 
				// ex : de lister.do si on clique sur ajouter on vise voirCreer.do donc on a /Struts/voirCreer.do
				// split => Split l'url avec / car getRequestURI donne /Struts/lister.do par exemple
				// 2 car on veux apr�s le 2eme / yep sa donne le lister.do avec l'exemple d'avant
				// le split de /Struts/voirCreer.do donne { "", "Struts" , "voirCreer.do"} Parce qu'avant le 1er / y a rien okay!

				// Redirection
				chain.doFilter(req, resp);
				return;
			}
		}

		// Retourne � lister.do si pas d'acc�s autoriser
		response.sendRedirect(request.getContextPath() + "/lister.do");
	}

	@Override
	public void init(final FilterConfig config) throws ServletException {
		// Empty block
	}
}