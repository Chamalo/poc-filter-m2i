package presentation.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * UtilisateurForm class; extends {@link org.apache.struts.action.ActionForm}
 * 
 * @author Valentin J.
 */
public class UtilisateurForm extends ActionForm {

	private static final long serialVersionUID = 328736578963851111L;
	private String            id;
	private String            nom;
	private String            prenom;
	private String            pwd;

	@Override
	public void reset(final ActionMapping mapping, final HttpServletRequest request) {
		super.reset(mapping, request);
	}

	@Override
	public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
		final ActionErrors errors = new ActionErrors();

		if (this.id.isEmpty()) {
			errors.add("id", new ActionMessage("errors.add.id"));
		} else {
			try {
				Integer.parseInt(this.id);
			} catch (final NumberFormatException n) {
				errors.add("id", new ActionMessage("errors.add.id.format"));
			}
		}

		if (this.nom.isEmpty()) {
			errors.add("nom", new ActionMessage("errors.add.nom"));
		}

		if (this.prenom.isEmpty()) {
			errors.add("prenom", new ActionMessage("errors.add.prenom"));
		}

		if (this.pwd.isEmpty()) {
			errors.add("pwd", new ActionMessage("errors.add.pwd"));
		}

		return errors;
	}

	/**
	 * Get the value of id
	 *
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Set new value to id
	 *
	 * @param id the id to set
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Get the value of nom
	 *
	 * @return the nom
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Set new value to nom
	 *
	 * @param nom the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Get the value of prenom
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return this.prenom;
	}

	/**
	 * Set new value to prenom
	 *
	 * @param prenom the prenom to set
	 */
	public void setPrenom(final String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Get the value of pwd
	 *
	 * @return the pwd
	 */
	public String getPwd() {
		return this.pwd;
	}

	/**
	 * Set new value to pwd
	 *
	 * @param pwd the pwd to set
	 */
	public void setPwd(final String pwd) {
		this.pwd = pwd;
	}

}
