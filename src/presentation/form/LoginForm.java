package presentation.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * LoginForm class; extends {@link org.apache.struts.action.ActionForm}
 * 
 * @author Valentin J.
 */
public class LoginForm extends ActionForm {

	private static final long serialVersionUID = 8396710189814774989L;
	private String            nom;
	private String            pwd;

	@Override
	public void reset(final ActionMapping mapping, final HttpServletRequest request) {
		super.reset(mapping, request);
	}

	@Override
	public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
		final ActionErrors errors = new ActionErrors();

		if (this.nom.isEmpty()) {
			errors.add("nom", new ActionMessage("errors.add.nom"));
		}

		if (this.pwd.isEmpty()) {
			errors.add("pwd", new ActionMessage("errors.add.pwd"));
		}

		return errors;
	}

	/**
	 * Get the value of nom
	 *
	 * @return the nom
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Set new value to nom
	 *
	 * @param nom the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * Get the value of pwd
	 *
	 * @return the pwd
	 */
	public String getPwd() {
		return this.pwd;
	}

	/**
	 * Set new value to pwd
	 *
	 * @param pwd the pwd to set
	 */
	public void setPwd(final String pwd) {
		this.pwd = pwd;
	}

}
