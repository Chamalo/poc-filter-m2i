package presentation.roles;

import java.util.Arrays;
import java.util.List;

/**
 * Enum for roles
 * 
 * @author Valentin J;
 */
public enum ERoles {
	/**
	 * Administrateur Role
	 */
	ADMIN(Arrays.asList("consulter.do", "voirCreer.do", "creer.do")),

	/**
	 * Client Role
	 */
	CLIENT(Arrays.asList("consulter.do"));

	/**
	 * List of all pages that role can access
	 */
	private List<String> pages;

	/**
	 * Constructor
	 *
	 * @param pages List of pages that role can access
	 */
	private ERoles(final List<String> pages) {
		this.pages = pages;
	}

	/**
	 * Get the value of pages
	 *
	 * @return the pages
	 */
	public List<String> getPages() {
		return this.pages;
	}

	/**
	 * Set new value to pages
	 *
	 * @param pages the pages to set
	 */
	public void setPages(final List<String> pages) {
		this.pages = pages;
	}

}
