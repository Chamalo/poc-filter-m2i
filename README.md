POC Filter pour le projet YNH.

On va appliquer un filtre sur certains écrans, afin de garantir l'accès ou non à certains utilisateurs.
Cet accès est déterminé par le rang de l'utilisateur.

Le rang est défini dans l'énumération ERang.
Chaque rang est initialisé avec une liste, qui indique à quel écran il a accès.

Le filtre UniqueFilter va vérifier si le rang de l'utilisateur lui accorde l'accès ou non.
Si l'accès n'est pas accordé, il est redirigé vers lister.do.

Identifiants pour les test : 
    Admin
        User 1
        test
    Client
        USer 2
        test
