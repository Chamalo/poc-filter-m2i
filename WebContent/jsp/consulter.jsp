<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html:html>
	<head>
		<meta charset="UTF-8">
		<title>
			<bean:message key="title.consult"/>
		</title>
	</head>
<body>

	<h1><bean:message key="consult.titre"/></h1>
	<br />
	
	<logic:present name="user">
		<ul>
			<li><bean:message key="consult.id"/> : ${user.id}</li>
			<li><bean:message key="consult.nom"/> : ${user.nom}</li>
			<li><bean:message key="consult.prenom"/> : ${user.prenom}</li>
			<li><bean:message key="consult.pwd"/> : ${user.pwd}</li>
			<li><bean:message key="consult.role"/> : ${user.role}</li>
		</ul>
	</logic:present>
	
	<logic:notPresent name="user">
		Utilisateur introuvable
	</logic:notPresent>

	<br>
	
	<html:link href="lister.do">	
		Retour
	</html:link>
	
	<html:link href="supprimer.do?id=${user.id}">	
		Supprimer
	</html:link>
	
	<html:link href="langue.do">	
		<bean:message key="message.language.list"/>
	</html:link>

</body>
</html:html>