<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="presentation.action.ListerAction" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html:html>
	<head>
		<meta charset="UTF-8">
		<title>
			<bean:message key="title.list"/>
		</title>
	</head>
<body>

	<h1>
		<bean:message key="message.welcome.list"/>
	</h1>

	<ul>
		<logic:iterate name="<%=ListerAction.LISTE_USERS_REQUEST%>" id="utilisateur">
			<html:link href="consulter.do?id=${utilisateur.id}">	
				<li>${utilisateur.id} - ${utilisateur.nom}</li>
			</html:link>	
		</logic:iterate>
	</ul>
	
	<html:link href="voirCreer.do">	
		Ajouter
	</html:link>
	
	<html:link href="langue.do">	
		<bean:message key="message.language.list"/>
	</html:link>
	
	<logic:present name="user">
		Bienvenue ${user.nom} - ${user.role}
		<html:link href="logout.do">
			Logout
		</html:link>
	</logic:present>
	
	<logic:notPresent name="user">
		<html:link href="voirLogin.do">
			Login
		</html:link>
	</logic:notPresent>
	
</body>
</html:html>