<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html:html>
	<head>
		<meta charset="UTF-8">
		<title>
			<bean:message key="title.create"/>
		</title>
	</head>
<body>

	<h1>
		<bean:message key="add.title"/>
	</h1>
	
	<html:errors/>
	
	<html:form action="/creer.do" focus="id">
		<table>
			<tr>
				<td>
					<label for="id">
						<bean:message key="add.create.id"/> : 
					</label>
				</td>
				<td>
					<html:text property="id" size="20" maxlength="20"/>
				</td>
				<td>
					<html:errors property="id" header="" prefix="" footer="" suffix=""/>
				</td>
			</tr>
			
			<tr>
				<td>
					<label for="nom">
						<bean:message key="add.create.nom"/> : 
					</label>
				</td>
				<td>
					<html:text property="nom" size="20" maxlength="20"/>
				</td>
				<td>
					<html:errors property="nom" header="" prefix="" footer="" suffix=""/>
				</td>
			</tr>
			
			<tr>
				<td>
					<label for="prenom">
						<bean:message key="add.create.prenom"/> : 
					</label>
				</td>
				<td>
					<html:text property="prenom" size="20" maxlength="20"/>
				</td>
				<td>
					<html:errors property="prenom" header="" prefix="" footer="" suffix=""/>
				</td>
			</tr>
			
			<tr>
				<td>
					<label for="pwd">
						<bean:message key="add.create.pwd"/> : 
					</label>
				</td>
				<td>
					<html:text property="pwd" size="20" maxlength="20"/>
				</td>
				<td>
					<html:errors property="pwd" header="" prefix="" footer="" suffix=""/>
				</td>
			</tr>
			
			<tr>
				<td><html:submit property="submit" value="Submit"/></td>
				<td><html:reset></html:reset></td>
				<td>
					<logic:messagesPresent message="true">
						<html:messages id="userOk" message="true">
							<bean:write name="userOk"/>
						</html:messages>
					</logic:messagesPresent>
				</td>
			</tr>
		</table>
	</html:form>
	
	<html:link href="lister.do">	
		Retour
	</html:link>
	
	<html:link href="langue.do">	
		<bean:message key="message.language.list"/>
	</html:link>
	
</body>
</html:html>