<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html:html>
	<head>
		<meta charset="UTF-8">
		<title>
			<bean:message key="title.login"/>
		</title>
	</head>
<body>

<html:errors/>
	
	<html:form action="/login.do" focus="nom">
		<table>
			<tr>
				<td>
					<label for="nom">
						<bean:message key="add.create.nom"/> : 
					</label>
				</td>
				<td>
					<html:text property="nom" size="20" maxlength="20"/>
				</td>
				<td>
					<html:errors property="nom" header="" prefix="" footer="" suffix=""/>
				</td>
			</tr>
			
			<tr>
				<td>
					<label for="pwd">
						<bean:message key="add.create.pwd"/> : 
					</label>
				</td>
				<td>
					<html:text property="pwd" size="20" maxlength="20"/>
				</td>
				<td>
					<html:errors property="pwd" header="" prefix="" footer="" suffix=""/>
				</td>
			</tr>
			
			<tr>
				<td><html:submit property="submit" value="Submit"/></td>
				<td><html:reset/></td>
				<td></td>
			</tr>
		</table>
	</html:form>
	
	<html:link href="lister.do">	
		Retour
	</html:link>
	
	<html:link href="langue.do">	
		<bean:message key="message.language.list"/>
	</html:link>
	
</body>
</html:html>